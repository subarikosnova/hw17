document.addEventListener("DOMContentLoaded", function () {

    const tabs = document.querySelectorAll('.tabs-title');
    const tabContents = document.querySelectorAll('.tab-content');


    tabs.forEach((tab, index) => {
        tab.addEventListener('click', () => {

            tabContents.forEach(content => content.style.display = 'none');

            tabContents[index].style.display = 'block';

            tabs.forEach(tab => tab.classList.remove('active'));
            tab.classList.add('active');
        });
    });
});